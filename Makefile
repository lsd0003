all: txt html

html:
	xml2rfc --html --css style.css draft-summermatter-set-union.xml

txt:
	xml2rfc draft-summermatter-set-union.xml
